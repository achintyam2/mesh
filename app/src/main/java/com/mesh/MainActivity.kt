package com.mesh

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mesh.model.Value
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val values = ArrayList<Value>()
    val valueAdapter : ValueAdapter by lazy { ValueAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ibBack.setOnClickListener {
            finish()
        }

        rvValues?.adapter = valueAdapter
        valueAdapter.swapData(feedData())

        valueAdapter.valueClicked = {
            valueAdapter.swapData(reset(it))
        }
    }

    private fun feedData() : ArrayList<Value>{
        return values.apply {
            add(Value("1","For newcomer",true))
            add(Value("100","For invitees only"))
            add(Value("200","Active users only"))
            add(Value("500","Active users only"))
            add(Value("1000","For all"))
            add(Value("1500","For all"))
            add(Value("2000","For all"))
            add(Value("5000","For all"))
            add(Value("8000","For all"))
        }
    }

    private fun reset(position : Int) : ArrayList<Value>{
        values.forEachIndexed { index, value ->
            value.selected = false
            if (position == index)
                value.selected = true
        }
        return  values
    }
}
