package com.mesh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mesh.model.Value
import kotlinx.android.synthetic.main.list_item_withdraw.view.*

class ValueAdapter() :
    ListAdapter<Value, ValueAdapter.MeterInfoViewHolder>(Statistics()) {
    private var values = ArrayList<Value>()
    var valueClicked = { _ : Int -> Unit}

    fun swapData(data: List<Value>) {
        this.values = data as ArrayList<Value>
        submitList(this.values.toMutableList())
        notifyDataSetChanged()
    }

    fun updateData(){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MeterInfoViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.list_item_withdraw, parent, false)
    )

    override fun onBindViewHolder(holder: MeterInfoViewHolder, position: Int) {holder.bind(getItem(position))}

    class Statistics : DiffUtil.ItemCallback<Value>() {
        override fun areItemsTheSame(oldItem: Value, newItem: Value)= oldItem.hashCode() == newItem.hashCode()

        override fun areContentsTheSame(oldItem: Value, newItem: Value) = oldItem.equals(newItem)
    }

    inner class MeterInfoViewHolder(itemView: View)
        : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            valueClicked(adapterPosition)
        }

        fun bind(item: Value) = with(itemView) {
            value.text = "₹".plus(item.value)
            userInfo.text = item.userInfo
            if (item.selected){
                tickLayout.visibility = View.VISIBLE
                rootLayout.background = resources.getDrawable(R.drawable.child_border_selected,null)
            }else{
                tickLayout.visibility = View.GONE
                rootLayout.background = resources.getDrawable(R.drawable.child_border_unselected,null)
            }
        }
    }
}


