package com.mesh.model

data class Value(
    var value: String,
    var userInfo: String,
    var selected: Boolean = false
)